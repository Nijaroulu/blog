//Add Member
var app = new Vue({
  el: '#AddUser',
  data: {
    //variable data will get updated during from submission, intially its null
    nickname:null,
    email:null,
    full_name:null,
    message:null,
    mainUrl:"http://127.0.0.1:8000/users/",//mailn url
  },
  methods: {
      AddMembers: function () {//method for adding new user
        this.message=null;
        axios
        //add new user to using post method
        .post(this.mainUrl, {
          "nickname": this.nickname,
          "email": this.email,
          "full_name":this.full_name,})
        .then(response => (this.message = response.statusText));
      }  
  },  
})
