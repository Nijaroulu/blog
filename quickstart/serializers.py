from django.contrib.auth.models import User, Group
from rest_framework import serializers
from quickstart.models import TUser, Appointment


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TUser
        fields = [
                'id',
                'nickname',
                'email',
                'full_name',
            ]

class AppointmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Appointment
        fields = [
                'id',
                'user',
                'date',
                'time_start',
                'time_end',
                'room_number',
            ]

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')