import uuid
from django.db import models
from django.conf import settings

class TUser(models.Model):
    nickname = models.CharField(max_length=100, unique=True)
    email = models.CharField(max_length=100, unique=True)
    full_name = models.CharField(max_length=100)
    date_joined = models.DateTimeField(null=True,auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    last_logged_in = models.DateTimeField(null=True, editable=False)



